// newkmer25.cpp : read entire fasta file, find k-mers in all strains
// compile with: g++ newkmer25.cpp -O3 -o newkmer
// MKM 5 AUG 2014

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
using namespace std;
#include <math.h>
#include <assert.h>
#include <string>
using namespace std;
#include <cstddef>

//input data files
const char drname[32] = "genomesdata.fasta"; //fasta file of all genomes of interest
const char dxname[32] = "candida.fasta";     //fasta file of sequences to exclude
const char iname[20] = "GenomeList.txt";   //text file of groups for probe design

const int KSIZE=25; //kmer size
const int MAXARRAY=120000000; //max number of nodes
const int NODESIZE=4;
const int MAXORGS=2048;

char oname[32] = "probes000.txt";
const int MAXTAR=999;
int *pNode; //pos 0 is A branch...3 is T 
int nctr=0;
int lctr=0; //all leaves
int cctr=0;
int tct=0;
int ntargorgs=0;FILE *fp;
bool printbool=false;
char probe[KSIZE];
char bases[4] = { 'A', 'C', 'G', 'T' };
int target;
string spmatch;
int mcount;

void TreeAdd(char *wd, int mode)
{
	int wpos, base, nodePtr, i;
	int *memPtr, *memPtr2;
	char fwd[32], rvs[32], c;
	char bases[4] = { 'A', 'C', 'G', 'T' };
	bool ok;

	for (i=0; i<KSIZE; i++) //only add in one direction -- "least val" direction
	{
		fwd[i]=wd[i];
		rvs[KSIZE-i-1]=3-wd[i]; //reverse complement
	}

	i=0;
	while (i<KSIZE && fwd[i]==rvs[i])
		i++;
	if (i<KSIZE && rvs[i]<fwd[i])
		for (i=0; i<KSIZE; i++)
			fwd[i]=rvs[i];

	nodePtr=0;
	wpos=-1;
	ok=true;
	while (++wpos<KSIZE-1 && ok)
	{ 
		base=fwd[wpos];
		memPtr=pNode+nodePtr*NODESIZE+base;
		if (*(memPtr)==0)
		{
			if (mode==1)
			{
				assert(nctr+1<MAXARRAY);
				*(memPtr) = ++nctr;
				memPtr2 = pNode+nctr*NODESIZE;
				for (i=0; i<NODESIZE; i++)
					*(memPtr2+i)=0;
			}
			else ok=false;
		}
		nodePtr=*(memPtr);
	}
	if (ok)
	{
		base=fwd[wpos];
		memPtr=pNode+nodePtr*NODESIZE+base;
		if (mode==1)
		{
			if (*(memPtr)==0)
				*(memPtr)=1;
			else 
				*(memPtr)=-1;
		}
		else if (mode==2)
		{

			if (*(memPtr)>0)
			{
				//cout << "OOO" << spmatch << endl;
				*(memPtr)=*(memPtr)+1;
				mcount++;
			}
		}
		else //3
		{
			//if (*(memPtr)>0)
			//	cout << "XXX" << spmatch << endl;
			*(memPtr)=-1;
		}
	}

}

void dfs(int nodePtr, int level)
{
	int i,j,k;
	int *memPtr, *memPtr2;
	int min;
	bool ok;
	char b1;

        //set minimum number of target genomes that must match
	if (ntargorgs == 1)
            min = 1;
	else if (ntargorgs<4)
	    min = 2;
        else min = ntargorgs-2;

	for (i=0; i<4; i++)
	{
		probe[level]=bases[i];
		memPtr=pNode+nodePtr*NODESIZE+i;
		if (*(memPtr)!=0)
		{
			if (level==KSIZE-1)
			{
				if (*(memPtr)>=min)
				{
		ok=true;
		j=1;
		b1=probe[0];
		while (b1==probe[j] && j<KSIZE-3)
			j++;
		if (j>5) ok=false;
		j=1;
		b1=probe[KSIZE-1];
		while (b1==probe[KSIZE-1-j] && j<KSIZE-3)
			j++;
		if (j>5) ok=false;
		if (ok)
		{
			tct++;
			if (printbool)
			{
				//fprintf(fp,"%d\t",target);
				for (j=0; j<KSIZE; j++)
					fprintf(fp,"%c",probe[j]);
				fprintf(fp,"\n");
			}
		}
				}
			}
			else 
				dfs(*(memPtr),level+1);
		}
	}

}


int main(int argc, char* argv[])
{

	char c1, word[32];
	char fname[32];
	int rdfl, cpos, i, j, n;
	int index, pindex;
	string accession[MAXORGS];
	string header;
	size_t found;
	int pfound;
	int norgs=0;
	int targno[MAXORGS], ref[MAXORGS];
	int ngen, nspec, ntarg;
	bool done;
	
	pNode=new int[4*MAXARRAY];
	//cout << "mem alloc " << endl;

	n=0;
	if (argc>1)
	{
		j=0;
		do
		{
			if ( *(argv[1]+j) )
			{
				c1 = *(argv[1]+j);
				n=n*10+c1-48;
				j++;
			}
			else
			{
				j=-1;
			}
		} while (j>0 && j<20); 
	}
	target=0;
	if (n>0 && n<MAXTAR)
	{
		target=n;
		cout << "target is " << target << endl;
	}

	if ((fp = fopen(iname, "rb"))==0) 
	{
		printf("Can't open data\n");
		exit(1);
	}
	do { 
		accession[norgs].clear();
		do
		{
			rdfl=fread(&c1, sizeof(char), 1, fp);
			if (c1>31)
				accession[norgs].append(1,c1);
		} while (c1!= 9 && rdfl);
		do
		{
			rdfl=fread(&c1, sizeof(char), 1, fp);
			//ignore strain name
		} while (c1!= 9 && rdfl);
		targno[norgs]=0;
		do
		{
			rdfl=fread(&c1, sizeof(char), 1, fp);
			if (c1>31)
				targno[norgs]=targno[norgs]*10+c1-48;
		} while (c1!= 9 && rdfl);
		ref[norgs]=0;
		do
		{
			rdfl=fread(&c1, sizeof(char), 1, fp);
			if (c1>31)
				ref[norgs]=ref[norgs]*10+c1-48;
		} while (c1!= 13 && c1!=10 && c1!=9 && rdfl);
		do
		{
			rdfl=fread(&c1, sizeof(char), 1, fp); //10 LF
		} while (c1!=10 && rdfl);
//cout << norgs << " " << targno[norgs] << " " << ref[norgs] << endl;
		if (rdfl) norgs++;

	} while (rdfl && norgs<MAXORGS);
	fclose(fp);
//cout << "norgs " << norgs << endl;

//for (target=1; target<10; target++)
//{

	memset(pNode,0,16*MAXARRAY);
	nctr=0;
	ntargorgs=0;
	int m = n;
        oname[6] = m/100 + 48;
        m = m%100;
	oname[7]=m/10+48;
	oname[8]=m%10+48;
	pindex=-1;
	done=false;

	if ((fp = fopen(drname, "rb"))==0) 
	{
		printf("Can't open data\n");
		exit(1);
	}
	cpos=0;
	do { //use ref sequences to start tree

		rdfl=fread(&c1, sizeof(char), 1, fp);
		if (c1=='>')
		{ //header
			header.clear();
			do
			{
				rdfl=fread(&c1, sizeof(char), 1, fp);
				if (c1>31)
					header.append(1,c1);
			} while (c1!= 13 && c1!=10 && rdfl);
			index=-1;

			for (i=0; i<norgs; i++)
			{
				found=header.find(accession[i]);
				pfound=(int)found;
				if (pfound<100 && pfound>-1)
				{
					index=i;
					break;
				}
			}
			if (done)
				index=0; //avoid dups!
			if (targno[index]==target && pindex!=index)
			{
				pindex=index;
if (ref[index]==1)
	cout << "1 " << index << " " << header << endl;

			}			
			cpos=0;

		}
		else if (c1=='A' || c1=='a')
		{
			word[cpos++]=0;
		}
		else if (c1=='C' || c1=='c')
		{
			word[cpos++]=1;
		}
		else if (c1=='G' || c1=='g')
		{
			word[cpos++]=2;
		}
		else if (c1=='T' || c1=='t')
		{
			word[cpos++]=3;
		}
		else if (c1=='-' || c1>='A' && c1<='Z' || c1>='a' && c1<='z')
		{
			cpos=0; //ambiguous character
		}
		if (cpos==KSIZE)
		{
			if (index>-1)
			{	

				if (ref[index]==1 && targno[index]==target && !done)
				{
					TreeAdd(word, 1);
					cpos=0; //no overlap! *************************
				}
			}
			if (cpos>0)
			{
				for (i=0; i<KSIZE-1; i++)
					word[i]=word[i+1]; //shift for next kmer
				cpos--;
			}
		}

	} while (rdfl);
	fclose(fp);
	pindex=-1;
	if ((fp = fopen(drname, "rb"))==0) 
	{
		printf("Can't open data\n");
		exit(1);
	}
	cpos=0;
	mcount=0;
	do { //add sequences to tree

		rdfl=fread(&c1, sizeof(char), 1, fp);
		if (c1=='>')
		{ //header
			header.clear();
			do
			{
				rdfl=fread(&c1, sizeof(char), 1, fp);
				if (c1>31)
					header.append(1,c1);
			} while (c1!= 13 && c1!=10 && rdfl);
			index=-1;

			for (i=0; i<norgs; i++)
			{
				found=header.find(accession[i]);
				pfound=(int)found;
				if (pfound<100 && pfound>-1)
				{
					index=i;
					break;
				}
			}
			cpos=0;
			if (index>-1)
			{
				if (targno[index]==target && pindex!=index)
				{
					ntargorgs++;
if (ref[pindex]==0 && pindex>-1)
	cout << mcount << " matches" << endl;
if (ref[index]==0)
	cout << "2 " << index << " " << header << endl;
					pindex=index;
					mcount=0;
				}
			}
		}
		else if (c1=='A' || c1=='a')
		{
			word[cpos++]=0;
		}
		else if (c1=='C' || c1=='c')
		{
			word[cpos++]=1;
		}
		else if (c1=='G' || c1=='g')
		{
			word[cpos++]=2;
		}
		else if (c1=='T' || c1=='t')
		{
			word[cpos++]=3;
		}
		else if (c1=='-' || c1>='A' && c1<='Z' || c1>='a' && c1<='z')
		{
			cpos=0; //ambiguous character
		}
		if (cpos==KSIZE)
		{
		if (index>-1)
		{	
			spmatch=accession[index];
			if (ref[index]==0 && targno[index]==target)
				TreeAdd(word, 2);
			else if (targno[index]!=target)
				TreeAdd(word, 3);
		}
			for (i=0; i<KSIZE-1; i++)
				word[i]=word[i+1]; //shift for next kmer
			cpos--;
		}

	} while (rdfl);
	fclose(fp);
if (ref[pindex]==0)
	cout << mcount << " matches" << endl;

	cout << "nodes in tree " << nctr << endl;
	cout << "number of target genomes " << ntargorgs << endl;
	tct=0;
	printbool=false;
	dfs(0,0);
	printf("target= %d, probe count = %d\n",target,tct);

	if ((fp = fopen(dxname, "rb"))==0) 
	{
		printf("Can't open data\n");
		exit(1);
	}
	cpos=0;
	do { //remove Candida or other excluded genomes

		rdfl=fread(&c1, sizeof(char), 1, fp);
		if (c1=='>')
		{ //header
			header.clear();
			do
			{
				rdfl=fread(&c1, sizeof(char), 1, fp);
				if (c1>31)
					header.append(1,c1);
			} while (c1!= 13 && c1!=10 && rdfl);
			cpos=0;
		}
		else if (c1=='A' || c1=='a')
		{
			word[cpos++]=0;
		}
		else if (c1=='C' || c1=='c')
		{
			word[cpos++]=1;
		}
		else if (c1=='G' || c1=='g')
		{
			word[cpos++]=2;
		}
		else if (c1=='T' || c1=='t')
		{
			word[cpos++]=3;
		}
		else if (c1=='-' || c1>='A' && c1<='Z' || c1>='a' && c1<='z')
		{
			cpos=0; //ambiguous character
		}
		if (cpos==KSIZE)
		{
			TreeAdd(word, 3);
			for (i=0; i<KSIZE-1; i++)
				word[i]=word[i+1]; //shift for next kmer
			cpos--;
		}

	} while (rdfl);
	fclose(fp);

	if ((fp = fopen(oname, "wb"))==0) 
	{
		printf("Can't open out\n");
		exit(1);
	}
	tct=0;
	printbool=true;
	dfs(0,0);
	printf("after exclusion, probe count = %d\n",tct);
	fclose(fp);

//} //next target

	delete[] pNode;


return 0;
}
