// k25read.cpp : read seq reads fasta file, match to kmers
// compile with: g++ k25read.cpp -O3 -o kread
// MKM 5 AUG 2014

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
using namespace std;
#include <math.h>
#include <assert.h>
#include <string>
using namespace std;
#include <cstddef>

//nctr = 42652806
//lctr = 3140132

const int MAXGRP=999;
const int KSIZE=25;
const int MAXARRAY=70000000; //max number of nodes
const int NODESIZE=4;
char dname[32] = "probes000.txt";
char rname[64];
char oname[32] = "result.txt";
int *pNode; //pos 0 is A branch...3 is T 
int gcount[MAXGRP], lcount[MAXGRP];
int nctr=0;
int lctr=0; //all leaves
int cctr=0;
int gct=0;
int ngroups=0;
void TreeAdd(char *wd, int group)
{
	int wpos, base, nodePtr, i;
	int *memPtr, *memPtr2;
	char bases[4] = { 'A', 'C', 'G', 'T' };

	nodePtr=0;
	wpos=-1;
	while (++wpos<KSIZE-1)
	{ 
		base=wd[wpos];;
		memPtr=pNode+nodePtr*NODESIZE+base;
		if (*(memPtr)==0)
		{
			assert(nctr+1<MAXARRAY);
			*(memPtr) = ++nctr;
			memPtr2 = pNode+nctr*NODESIZE;
			for (i=0; i<NODESIZE; i++)
				*(memPtr2+i)=0;
		}
		nodePtr=*(memPtr);
	}
		base=wd[wpos];
		memPtr=pNode+nodePtr*NODESIZE+base;
		lctr++;
		if (*(memPtr) ==0)
			*(memPtr) = group;
		else if (*(memPtr) !=group)
			*(memPtr) = -1;

}

bool TreeSearch(char *wd)
{
	int wpos, base, nodePtr, i;
	int *memPtr, *memPtr2;
	int group;
	char fwd[32], rvs[32];
	char bases[4] = { 'A', 'C', 'G', 'T' };
	bool ok=true;

	for (i=0; i<KSIZE; i++) //only add in one direction -- "least val" direction
	{
		fwd[i]=wd[i];
		rvs[KSIZE-i-1]=3-wd[i]; //reverse complement
	}
	i=0;
	while (i<KSIZE && fwd[i]==rvs[i])
		i++;
	if (i<KSIZE && rvs[i]<fwd[i])
		for (i=0; i<KSIZE; i++)
			fwd[i]=rvs[i];

	nodePtr=0;
	wpos=0;
	do
	{ 
		base=fwd[wpos];
		memPtr=pNode+nodePtr*NODESIZE+base;
		nodePtr=*(memPtr);
	} while (++wpos<KSIZE && nodePtr>0);

	if (nodePtr>0)
	{
		group=nodePtr;
		lcount[group]++;
		//*(memPtr)=0; //****************************non-redundant hits
	}

return ok;

}

int main(int argc, char* argv[])
{
	FILE *fp;
	char c1, word[32];
	char fname[32];
	int rdfl, cpos, i,j;
	string header;
	size_t found;
	int maxv, maxi, sum;
	bool ok;

	j=0;
	if (argc>1)
	{
		j=0;
		do
		{
			if ( *(argv[1]+j) )
			{
				c1 = *(argv[1]+j);
				rname[j]=c1;
				j++;
			}
			else
			{
				i=j;
				j=-1;
			}
		} while (j>0 && j<64); 
	}

	pNode=new int[4*MAXARRAY];
	pNode[0]=0;
	pNode[1]=0;
	pNode[2]=0;
	pNode[3]=0;

ok=true;
ngroups=1;
while (ok)
{
	int m = ngroups;
        dname[6] = m/100 + 48;
        m = m%100;
	dname[7]=m/10+48;
	dname[8]=m%10+48;
	if ((fp = fopen(dname, "rb"))==0) 
	{
		printf("read %d groups\n",ngroups-1);
		ok=false;
	}
	else
	{
		cpos=0;
		do { //use sequences to make tree

		rdfl=fread(&c1, sizeof(char), 1, fp);
		if (c1=='A' || c1=='a')
		{
			word[cpos++]=0;
		}
		else if (c1=='C' || c1=='c')
		{
			word[cpos++]=1;
		}
		else if (c1=='G' || c1=='g')
		{
			word[cpos++]=2;
		}
		else if (c1=='T' || c1=='t')
		{
			word[cpos++]=3;
		}
		else
		{
			cpos=0; 
		}
		if (cpos==KSIZE && rdfl)
		{
			TreeAdd(word, ngroups);
		}
		} while (rdfl);
		fclose(fp);
		if (++ngroups >= MAXGRP)
			ok=false;
	} //file opened ok
} //groups
	printf("nodes in tree %d\n",nctr);

	for (i=0; i<ngroups; i++)
	{
		gcount[i]=0;
		lcount[i]=0;
	}	
	if ((fp = fopen(rname, "rb"))==0) 
	{
		printf("Can't open read data\n");
		exit(1);
	}
	cpos=0;
	do { //process reads file

		rdfl=fread(&c1, sizeof(char), 1, fp);
		if (c1=='>')
		{ //header
			maxv=0;
			maxi=0;
			sum=0;
			for (i=0; i<ngroups; i++)
			{
				if (lcount[i]>maxv)
				{
					maxv=lcount[i];
					maxi=i;
				}
				sum+=lcount[i];
				lcount[i]=0;
			}

			if (maxv*10<sum*6)
				maxi=0;
			gcount[maxi]++;

			header.clear();
			do
			{
				rdfl=fread(&c1, sizeof(char), 1, fp);
				if (c1>31)
					header.append(1,c1);
			} while (c1!= 10 && rdfl);
			cpos=0;
		}
		else if (c1=='A' || c1=='a')
		{
			word[cpos++]=0;
		}
		else if (c1=='C' || c1=='c')
		{
			word[cpos++]=1;
		}
		else if (c1=='G' || c1=='g')
		{
			word[cpos++]=2;
		}
		else if (c1=='T' || c1=='t')
		{
			word[cpos++]=3;
		}
		else if (c1=='-' || c1>='A' && c1<='Z' || c1>='a' && c1<='z')
		{
			cpos=0; //ambiguous character
		}
		if (cpos==KSIZE)
		{
			ok=TreeSearch(word);

			for (i=0; i<KSIZE-1; i++)
				word[i]=word[i+1]; //shift for next kmer
			cpos--;
		}

	} while (rdfl);
	fclose(fp);

	delete[] pNode;


	if ((fp = fopen(oname, "wb"))==0) 
	{
		printf("Can't open out\n");
		exit(1);
	}
	for (i=0; i<ngroups; i++)
		fprintf(fp,"%d,%d\n",i,gcount[i]);

	fclose(fp);

return 0;
}
